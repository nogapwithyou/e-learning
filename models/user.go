package models

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
	"github.com/dgrijalva/jwt-go"
)

// UserList //
var (
	UserList map[string]*User
)

func init() {
	UserList = make(map[string]*User)
	// u := User{
	// 	"user_11111",
	// 	"admin",
	// 	"admin",
	// 	Profile{
	// 		"male",
	// 		25,
	// 		"Indonesia",
	// 		"admin@gmail.com",
	// 	}}
	// UserList["user_11111"] = &u

	// Need to register model in init
	orm.RegisterModel(new(User))
}

// User //
type User struct {
	ID        int       `orm:"null;auto;pk"`
	UserID    string    `orm:"null"`
	Username  string    `valid:"MinSize(5)"`
	Password  string    `valid:"MinSize(8)"`
	Gender    string    `orm:"null"`
	Age       int       `orm:"null"`
	Address   string    `orm:"null"`
	Email     string    `valid:"Email; MaxSize(100)"`
	CreatedAt time.Time `orm:"auto_now_add;type(datetime)"`
	UpdatedAt time.Time `orm:"auto_now;type(datetime);null"`
}

// Profile //
type Profile struct {
	ID      int    `orm:"null;auto;pk"`
	Gender  string `orm:"null"`
	Age     int    `orm:"null"`
	Address string `orm:"null"`
	Email   string `valid:"Email; MaxSize(100)"`
	// User    *User  `orm:"reverse(one)"`
}

// LoginDetail //
type LoginDetail struct {
	// ID       int `orm:"null;auto;pk"`
	UserID    string
	Token     string
	ExpiresAt time.Time
	CreateAt  time.Time `orm:"auto_now_add;type(datetime)"`
}

// TokenClaims - Claims for a JWT access token.
type TokenClaims struct {
	UserID string
	jwt.StandardClaims
}

// GetMD5Hash = Encode password //
func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

// CreateToken - Create a JWT access token.
func CreateToken(UserID string) (string, error) {

	expireToken := time.Now().Add(time.Minute * 5).Unix()
	log.Println("TOKEN STRING", UserID)
	// Set-up claims
	claims := TokenClaims{
		UserID: UserID,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expireToken,
			Issuer:    "bandzest-auth",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString([]byte("JANCOK"))

	return tokenString, err
}

//Valid - validation user
func (user *User) Valid(v *validation.Validation) {
	if strings.Index(user.Username, "admin") != -1 {
		// Set error messages of Name by SetError and HasErrors will return true
		v.SetError("Name", "Can't contain 'admin' in Name")
	}
}

// AddUser = Add New User //
func AddUser(newUser User) (uu *User, err error) {
	o := orm.NewOrm()
	valid := validation.Validation{}
	var user *User
	var error error

	val, err := valid.Valid(&newUser)
	if err != nil {
		user = nil
		error = err
	}
	if !val {
		// validation does not pass
		// blabla...
		for _, err := range valid.Errors {
			log.Println(err.Key, err.Message)
			user = nil
			error = errors.New(err.Message)
		}
	} else {
		newUser.UserID = "user_" + strconv.FormatInt(time.Now().UnixNano(), 10)
		// fmt.Println("PASSWORD REGISTER", newUser.Password)
		newUser.Password = GetMD5Hash(newUser.Password)

		// Three return values：Is Created，Object Id，Error
		created, id, err := o.ReadOrCreate(&newUser, "Username")
		if err == nil {
			if created {
				fmt.Println("New Insert an object. Id:", id)
				user = &newUser
				error = nil
			} else {
				fmt.Println("Get an object. Id:", id)
				user = nil
				error = errors.New("User Already Exist")
			}
		} else {
			user = nil
			error = err
		}
	}

	return user, error
}

// Login user = use JWT //
func Login(loginUser User) (detail *LoginDetail, err error) {
	o := orm.NewOrm()
	// var LoginDetail *LoginDetail
	LoginDetail := new(LoginDetail)

	loginUser.Password = GetMD5Hash(loginUser.Password)
	expireToken := time.Now().Add(time.Minute * 5)

	qs := o.QueryTable("user")
	result := qs.Filter("Password", loginUser.Password).Filter("Username", loginUser.Username).Exist()
	fmt.Println("EXIST", result)

	if result {

		userQuery := o.Read(&loginUser, "Username")

		if userQuery == orm.ErrNoRows {
			fmt.Println("No result found.")
		} else if userQuery == orm.ErrMissPK {
			fmt.Println("No primary key found.")
		} else {
			fmt.Println(loginUser.UserID, loginUser.Username)

			LoginDetail.UserID = loginUser.UserID
			LoginDetail.Token, err = CreateToken(loginUser.UserID)
			LoginDetail.ExpiresAt = expireToken
			LoginDetail.CreateAt = time.Now()

			LoginDetail = LoginDetail
			err = nil
		}
	} else {
		LoginDetail = nil
		err = errors.New("User not exist")
	}

	return LoginDetail, err
}

// GetUser = get Current User //
func GetUser(uid string) (u *User, err error) {
	if u, ok := UserList[uid]; ok {
		return u, nil
	}
	return nil, errors.New("User not exist")
}

package main

import (
	_ "E-Learning/routers"

	_ "github.com/lib/pq"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

func init() {
	orm.RegisterDriver("pq", orm.DRPostgres)
	orm.RegisterDataBase(
		"default",
		"postgres",
		"user=postgres password=admin dbname=E-Learning sslmode=disable",
	)
	// Database alias.
	// name := "default"

	// // Drop table and re-create.
	// force := true

	// // Print log.
	// verbose := true

	// // Error.
	// err := orm.RunSyncdb(name, force, verbose)
	// if err != nil {
	// 	fmt.Println(err)
	// }
}

func main() {
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}
	beego.Run()
}

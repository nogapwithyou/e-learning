package routers

import (
	"E-Learning/controllers"

	"github.com/astaxie/beego"
)

func init() {
	ns := beego.NewNamespace("/v1",
		beego.NSNamespace("/user",
			beego.NSRouter("/register", &controllers.UserController{}, "post:Register"),
			beego.NSRouter("/login", &controllers.UserController{}, "post:Login"),
		),
	)
	beego.AddNamespace(ns)
}

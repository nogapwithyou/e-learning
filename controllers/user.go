package controllers

import (
	"E-Learning/models"
	"encoding/json"

	"github.com/astaxie/beego"
)

// UserController = Operations about Users
type UserController struct {
	beego.Controller
}

// Register = function to AddUser
// @Title Register
// @Description create users
// @Param	body		body 	models.User	true		"body for user content"
// @Success 200 {int} models.User.ID
// @Failure 403 body is empty
// @router /v1/user/register [post]
func (u *UserController) Register() {
	var user models.User
	var response = json.Unmarshal(u.Ctx.Input.RequestBody, &user)

	// if response == nil {
	// 	beego.Debug(user)
	// 	u.Data["json"] = user
	// 	u.ServeJSON()
	// } else {
	// 	beego.Debug("error: ", response)
	// }
	if response == nil {
		uu, err := models.AddUser(user)
		if err != nil {
			beego.Debug("ERROR", err)
			u.Data["json"] = err.Error()
		} else {
			beego.Debug(uu)
			u.Data["json"] = uu
		}
		u.ServeJSON()
	} else {
		beego.Debug("error: ", response)
	}
}

// Login = function to Login
// @Title Login
// @Description Logs user into the system
// @Param	username		query 	string	true		"The username for login"
// @Param	password		query 	string	true		"The password for login"
// @Success 200 {string} login success
// @Failure 403 user not exist
// @router /v1/user/login [post]
func (u *UserController) Login() {
	var user models.User
	var response = json.Unmarshal(u.Ctx.Input.RequestBody, &user)

	if response == nil {
		detail, err := models.Login(user)
		if err != nil {
			u.Data["json"] = err.Error()
		} else {
			u.Data["json"] = detail
		}
		u.ServeJSON()
	} else {
		beego.Debug("error: ", response)
	}
}
